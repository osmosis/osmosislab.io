# Software architecture

The functionnal architecture contains mainly two functions:
* a _navigation_ functionality used to move the robot on the airport to reach runway extremities;
* a _line following_ functionality used to follow the lights line at a constant speed
  for the lighting control.

The software architecture is based on the control architecture embedded on
an ONERA platform, Robotnik Summit-XL. The robot is
equipped with a IMU (Inertial Measurement Unit), a GPS sensor, Hokuyo
laser sensors and a video camera.

<img src="../img/summit-onera.jpg" width="300"/>
<img src="../img/summit-size.png" width="300"/>
