# Experimental logs

The OSMOSIS experiment also comes with real logs gathered on our robot. They have not
been gathered on an airport light inspection, but they may allow you to test some
safety methods based on real data analysis.

Logs are available as ROS bags, which give the possibility to easily replay them
in place of using the simulator. For instance, using the [ROS architecture](ros.md),
once the ROS architecture is launched, you can replay a ROS bag using `rosbag play`.
Of course, the command computed by your architecture will not been applied.

For the moment, three logs are available (the trajectory done by the robot is shown on the associated figure):
 * <a href="../bags/osmosis_2018-05-23-18-54-32.bag" target="_blank">osmosis_2018-05-23-18-54-32.bag</a>
 <img width=200 src="../bags/osmosis_2018-05-23-18-54-32_traj.png"/>
 * <a href="../bags/osmosis_2018-05-23-18-55-50.bag" target="_blank">osmosis_2018-05-23-18-55-50.bag</a>
 <img width=200 src="../bags/osmosis_2018-05-23-18-55-50_traj.png"/>
 * <a href="../bags/osmosis_2018-05-23-19-07-52.bag" target="_blank">osmosis_2018-05-23-19-07-52.bag</a>
 <img width=200 src="../bags/osmosis_2018-05-23-19-07-52_traj.png"/>

These bags contain all the sensors data (IMU, GPS, odometry, laser scans) as well
as the command applied to the robot.
