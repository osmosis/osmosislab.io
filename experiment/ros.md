# OSMOSIS ROS implementation

The software architecture of the OSMOSIS experiment has been implemented
using the [ROS](http://www.ros.org) middleware to provide a simple reference implementation.
The source code of this architecture is available on repository https://gitlab.com/osmosis/osmosis_control_ros.
A complete class diagram showing ROS nodes and topics is here : [UML Class diagram of ROS Osmosis controller](../../img/ClassDiagRos.pdf)


## Installation

The following procedures is given for Ubuntu 18.04 and ROS Melodic.
You must first follow the installation instructions of [ROS](http://www.ros.org).

You should also follow the instructions to install the [OSMOSIS simulation](simulation.md) environment.

### Workspace

Create a ROS workspace for the OSMOSIS experiment (or use the OSMOSIS simulation workspace),
and clone the necessary repositories:

```
source /opt/ros/melodic/setup.bash
mkdir -p ~/osmosis_ws/src
cd ~/osmosis_ws/src
git clone https://gitlab.com/osmosis/osmosis_control_ros
vcs import < osmosis_control_ros/osmosis.repos
cd ..
catkin_make --pkg osmosis_control_msgs
catkin_make
```

## Launch the nodes

```
cd osmosis_ws
source devel/setup.bash
roslaunch osmosis_control_ros osmosisControl.launch
```

## Controlling the robot

If you also launch the [simulation environment](simulation.md), then you can give
a goal into the navigation console, and the robot should go (press the Play button in the simulator).
