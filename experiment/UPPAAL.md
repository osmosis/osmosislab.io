# OSMOSIS UPPAAL properties.

The current implementation of the UPPAAL model and the properties checked with GenoM3 can be obtained [here](https://redmine.laas.fr/projects/case-study-osmosis/repository/revisions/master/entry/README).
