# OSMOSIS GenoM3 implementation

The current implementation of the  OSMOSIS project in [GenoM3](https://git.openrobots.org/projects/genom3?jump=welcome) can be obtained [here](https://redmine.laas.fr/projects/osmosis?jump=welcome/).
