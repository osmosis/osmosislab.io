# OSMOSIS Experiment

The OSMOSIS experiment is inspired from the
[SafeAM experiment](http://www.cpse-labs.eu/experiment.php?id=c1_fr_safeam)
supported by the France design center as part of the
[CPSE Labs](http://www.cpse-labs.eu) project.

The experiment is about an autonomous robot performing a light inspection mission
on an airport. Indeed, according to the civil aviation regulation, the light
intensity on airfields is to be measured and determines whether or not the
runway can be open to air traffic. Currently, human maintenance operators
perform the light inspection but this is a burdensome and unpleasant task.
It has to be done late at night, typically from 1am to 4am. The repeated
exposure to intense lights in surrounding dark may cause eye strain.
The task would thus be a perfect fit for robotic operation. However, today,
no robot is authorized on airports due to safety issues.

In the OSMOSIS experiment, we consider an autonomous robot performing such a mission.
It has to navigate on the airport in order to reach runways, and then proceed
to light inspections by driving along the light row and grabbing light intensity
data using a specific sensor.

OSMOSIS provides a simulation environment to run such a mission, as well as
some control software. 
