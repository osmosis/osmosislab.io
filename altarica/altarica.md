# AltaRica - a high level modeling language dedicated to Safety Analyses

For more information and publications, see http://altarica-association.org/index.html and https://altarica.labri.fr/wp/.

The OSMOSIS model _LightIspectionV5_
describes the safety procedures achieved to manage hazards during the light inspection with the robot.
Associated files:
  * source file: <a href="LightInspectionV5.alt" target="_blank">LightIspectionV5.alt</a>.
  * report: <a href="LightInspectionV5.pdf" target="_blank">LightIspectionV5.pdf</a>.

It has been developed and analyzed with Cecilia Workbench from Dassault Aviation.

The analysis aims at computing combinations of technical failures or human error leading to several undesired events. Analysis results are given in the following files:
  * <a href="CollisionWithAircraftSeq5.seq" target="_blank">CollisionWithAircraftSeq5.seq</a>
  * <a href="CollisionWithObstacleSeq5.seq" target="_blank">CollisionWithObstacleSeq5.seq</a>
  * <a href="DegradedRobotOperationSeq5.seq" target="_blank">DegradedRobotOperationSeq5.seq</a>
  * <a href="DegradedSupervisionSeq5.seq" target="_blank">DegradedSupervisionSeq5.seq</a>
  * <a href="IntempestiveMissionStopSeq5.seq" target="_blank">IntempestiveMissionStopSeq5.seq</a>
