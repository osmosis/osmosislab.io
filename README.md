<!--
 ![Build Status](https://gitlab.com/pages/gitbook/badges/pages/build.svg)
-->
<p align="center" >
<img src="../img/OSMOSIS-logov3.jpg"/ width="1000">
</p>

# Open-Source Material fOr Safety assessment of Intelligent Systems

---

## What is OSMOSIS?

The OSMOSIS project objective is to provide some open-source
material in order to illustrate methods or platforms aimed at assessing,
or implementing safety for autonomous, intelligent systems.

OSMOSIS contains:
* some documentation, presenting the mission description, and some requirements,
* a description of the functionnal architecture embedded on the robot,
* the source code corresponding to an implementation of this architecture using the ROS middleware,
* a simulator in order to be able to run this architecture,
* data coming from real experiments done using a robot from ONERA.

OSMOSIS has been developed as part of the
[CPSE-Labs](http://www.cpse-labs.eu/) project.

<p align="left" >
  <img src="../img/cpse_labs_logo.png"/ width="200">
</p>
