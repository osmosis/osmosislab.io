Osmosis project is currently supported/developped by:

* Charles Lesire  - ONERA, Toulouse, France, 
    https://www.onera.fr/staff/charles-lesire

* Félix Ingrand - LAAS-CNRS, Toulouse, France, 
    https://homepages.laas.fr/felix/

* Jérémie Guiochet - LAAS-CNRS, Toulouse, France, 
    http://homepages.laas.fr/guiochet/