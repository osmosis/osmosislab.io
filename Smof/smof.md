# Safety Monitoring Framework (SMOF)

SMOF is a research project by LAAS-CNRS to assist the specification of safety rules for autonomous systems. SMOF is a framework to assist the specification of safety rules executed by an independent monitor to ensure safety of the whole system. The safety rules are high-level requirements of the monitor expressed in terms of observable variables on the system and its environment and interventions.

An application of SMOF to Osmosis can be found here : https://www.laas.fr/projects/smof/
(See "Case Studies" section)