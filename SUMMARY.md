# Summary

* [What is OSMOSIS?](README.md)
* [OSMOSIS Experiment](experiment/README.md)
  * [Software architecture description](experiment/architecture.md)
  * [Simulation environment](experiment/simulation.md)
  * [ROS implementation](experiment/ros.md)
  * [GenoM3 implementation](experiment/GenoM3.md)
  * [Experimental logs](experiment/logs.md)
* [Safety assessment methods](methods/README.md)
  * [AltaRica](altarica/altarica.md)
  * [GenoM3 UPPAAL properties](experiment/UPPAAL.md)
  * [Safety Monitoring Framework (SMOF)](Smof/smof.md)
* [Contact](CONTACT.md)
